import {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import * as AppAuth from 'expo-app-auth';
import {ApplicationState} from '../store/index';
import {Token} from '../store/auth/type';
import { setToken } from '../store/auth/action';
import {config as AuthConfig} from '../constants/authProvider';
import jwtDecode  from 'jwt-decode';


const host = process.env.REACT_APP_API_URL || "http://esgi-pa-api-261421812.eu-west-3.elb.amazonaws.com";


export const useFetchWithToken = () => {

    const dispatch = useDispatch();
    const token = useSelector<ApplicationState>(state => state.auth.token) as Token;

    async function refreshToken(token: Token){

        if(token.refresh_token){

            try{

                const newState = await AppAuth.refreshAsync(AuthConfig, token.refresh_token)
    
                if(newState.refreshToken && newState.accessToken)
                dispatch(setToken(
                    newState.refreshToken && newState.accessToken 
                        ?
                            {
                                isAuth: true,
                                expires: newState.accessTokenExpirationDate ? newState.accessTokenExpirationDate : new Date().toString(),
                                refresh_token: newState.refreshToken,
                                token: newState.accessToken,
                                tokenDecode: jwtDecode(newState.accessToken)
                            }
                        :
                            { isAuth: false }
                ))

                return newState.accessToken
            }catch(e){

                dispatch(setToken({
                    isAuth: false
                }))

                throw new Error("Invalid Auth");
            }

            
        }

        return token.token;
    }

    function checkTokenRefresh(token: Token){

        if(token.expires && new Date(token.expires) < new Date()){
            
            return true;
        }

        return false;
    }
    

    return (url: string, conf?: RequestInit) => {

        return new Promise(async (resolve, reject) => {

            if(checkTokenRefresh(token)){

                try{

                    const newToken = await refreshToken(token);

                    return resolve(newToken);
                }catch (e){

                    reject(e);
                }   
            }

            return resolve(token.token)

        }).then(currentToken => {
            
            return fetch(
                `${host}${url}`, 
                conf === undefined 
                    ? {
                        headers: {
                            'Authorization': `Bearer ${currentToken}`
                        }
                    } 
                    : {
                        ...conf,
                        headers: {
                            ...conf.headers,
                            'Authorization': `Bearer ${currentToken}`
                        }
                    })
        }).then(response => {

            if(response.status === 401)

                dispatch(setToken({
                    isAuth: false
                }))

            return response;
        })
        
    }
}