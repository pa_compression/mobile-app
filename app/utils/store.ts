import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import {rootReducer} from '../store/index';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}

const persistedReducer = persistReducer(persistConfig, rootReducer); 

const store = createStore(
    persistedReducer,
    // applyMiddleware (
    //     createLogger()
    // )
)

const persistor = persistStore(store);

export {
    store,
    persistor
}