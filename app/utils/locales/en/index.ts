export default {
    route: {
        bottomBar: {
            FOLDER: "Folder",
            SETTINGS: "Settings",
            TASK: "Task"
        },
        DEFAULT: "Home",
        bottomBarHeader: {
            FOLDER: "My folders",
            SETTINGS: "My settings",
            TASK: "My tasks"
        },
        TASK_DETAIL: "Detail task",
        TASK_CREATE: "Create",
        FILE_DETAIL: "Detail file",
        DOWNLOAD_FILE: "Download file"
    },
    field: {
        string: {
            url: "Set a valid url"
        }
    },
    taskStatus: {
        ERROR: "Erreur",
        RUNNING: "En cours",
        WAITING: "En attente",
        END: "Terminé"
    },
    INVALID_FORM: "Invalid form",
    LOADING: "Loading your content",
    WELCOME: "Welcome",
    LOGIN_MESSAGE : "Welcome to TCD mobile app. To continue please login",
    LOGIN: "Sign in / up",
    LOGIN_TITLE: "Sign in",
    LOGOUT: "Logout",
    ACCOUNT: "Account",
    AUTHENTICATION: "Authentication",
    EMAIL: "Email",
    NAME: "Name",
    EXPIRES: "Expires",
    RUNNING: "Running",
    WAITING: "Waiting",
    END: "Finish",
    USER: "User",
    STATUS: "Status",
    STATE: "State",
    PROGRESSION: "Progression",
    CREATEDAT: "Created at",
    STARTEDAT: "Started at",
    CREATE: "Create",
    DONE: "Done",
    ERROR: "An error occured",
    ERROR_SAVE_DATA: "Can't save your data",
    NO_FILE: "No file",
    NO_TASK: "No task",
    ERROR_TITLE: "Error",
    TYPE: "Type",
    UNCOMPRESS: "Uncompress",
    COMPRESS: "Compress",
    TASK_COMPRESS: "Compress task",
    TASK_UNCOMPRESS: "Uncompress task",
    DOWNLOAD_COMPRESS: "Download compress file",
    DOWNLOAD_UNCOMPRESS: "Download uncompress file",
    SHARE_COMPRESS: "Share compress file",
    UNCOMPRESS_TASK_CREATE_TITLE: "Uncompress ask",
    UNCOMPRESS_TASK_CREATE_TEXT: "Your uncompress task has been send",
    UNCOMPRESS_TASK_CREATE_ERROR_TITLE: "Error",
    UNCOMPRESS_TASK_CREATE_ERROR_TEXT: "Your uncompress task has failed",
    TASK_DETAIL: "Detail task",
    DOWNLOAD_MY_FILE: "Download my file: ",
    WARNING: "Warning",
    DELETE_FILE_WARNING: "You gonna delete all the task and file link to this file",
    DELETE_ERROR_TEXT: "Cannot delete your project",
    DELETE: "Delete"
}