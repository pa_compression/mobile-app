export default {
    route: {
        DEFAULT: "Accueil",
        bottomBar: {
            FOLDER: "Dossiers",
            SETTINGS: "Paramètres",
            TASK: "Tâche"
        },
        bottomBarHeader: {
            FOLDER: "Mes dossiers",
            SETTINGS: "Mes paramètres",
            TASK: "Mes tâches"
        },
        TASK_DETAIL: "Détail tâche",
        TASK_CREATE: "Ajouter",
        FILE_DETAIL: "Détail fichier",
        DOWNLOAD_FILE: "Télécharger fichier"
    },
    field: {
        string: {
            url: "Saisissez une url valide"
        }
    },
    taskStatus: {
        ERROR: "Erreur",
        RUNNING: "En cours",
        WAITING: "En attente",
        END: "Terminé"
    },
    INVALID_FORM: "Formulaire non valide",
    LOADING: "Chargement de votre contenue",
    WELCOME: "Bienvenue",
    LOGIN_MESSAGE : "Bienvenue sur l'application mobile de TCD. Pour continuer merci de vous authentifier",
    LOGIN: "Connexion / S'enregister",
    LOGIN_TITLE: "Connexion",
    LOGOUT: "Déconnexion",
    ACCOUNT: "Compte",
    AUTHENTICATION: "Authentification",
    EMAIL: "Mail",
    NAME: "Nom",
    EXPIRES: "Expire",
    RUNNING: "En cours",
    WAITING: "En attente",
    END: "Terminé",
    USER: "Utilisateur",
    STATUS: "Statut",
    STATE: "Etat",
    PROGRESSION: "Progression",
    CREATEDAT: "Crée le",
    STARTEDAT: "Démarré le",
    CREATE: "Ajouter",
    DONE: "OK",
    ERROR: "Une erreur est survenue",
    ERROR_SAVE_DATA: "Impossible de sauvegarder vos données",
    NO_FILE: "Aucun fichier",
    NO_TASK: "Aucune tâche",
    ERROR_TITLE: "Erreur",
    TYPE: "Type",
    UNCOMPRESS: "Décompression",
    COMPRESS: "Compression",
    TASK_COMPRESS: "Tâche compression",
    TASK_UNCOMPRESS: "Tâche décompression",
    DOWNLOAD_COMPRESS: "Télécharger fichier compressé",
    SHARE_COMPRESS: "Partager fichier compressé",
    DOWNLOAD_UNCOMPRESS: "Télécharger fichier décompressé",
    UNCOMPRESS_TASK_CREATE_TITLE: "Décompression démandée",
    UNCOMPRESS_TASK_CREATE_TEXT: "Votre demande de décompression a été envoyé",
    UNCOMPRESS_TASK_CREATE_ERROR_TITLE: "Erreur",
    UNCOMPRESS_TASK_CREATE_ERROR_TEXT: "Votre demande de décompression a échoué",
    DOWNLOAD_MY_FILE: "Télécharge mon fichier à cette adresse: ",
    WARNING: "Attention",
    DELETE_FILE_WARNING: "Vous allez supprimer tous les fichiers et tâches liées à ce projet",
    DELETE_ERROR_TEXT: "Impossible de supprimer votre projet",
    DELETE: "Supprimer"
}