import {OAuthBaseProps} from 'expo-app-auth';

export default async function RevokeAccessToken({clientId, serviceConfiguration}: OAuthBaseProps, token:string){
    const encodedClientID = encodeURIComponent(clientId);
    const encodedToken = encodeURIComponent(token);
    const body = `refresh_token=${encodedToken}&client_id=${encodedClientID}`;
    const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
    if(!serviceConfiguration || !serviceConfiguration.revocationEndpoint)

        throw Error('No revocation endpoint provide');

    try {
        // https://tools.ietf.org/html/rfc7009#section-2.2
        const results = await fetch(serviceConfiguration.revocationEndpoint, {
            method: 'POST',
            headers,
            body,
        });
        //console.log(results.status);
        return results;
    }
    catch (error) {
        //console.log("logout error");
        //console.log(error.message);
        throw new Error(error.message);
    }
}

