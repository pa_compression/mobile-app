import React, { useEffect, useState } from 'react';
import { View, Text, ActivityIndicator, StyleSheet, SectionList, SectionListData } from 'react-native';
import { useTheme } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { TaskType, TaskStatus } from '../constants/service';
import { useFetchWithToken } from '../utils/query';
import { RootStackParamList } from '../constants/navigation';
import ErrorLoading from '../components/ErrorLoading';
import i18n from '../utils/i18n';


type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'TASK_DETAIL'
>;

type Props = {
  navigation: ProfileScreenNavigationProp;
  route: RouteProp<RootStackParamList, 'TASK_DETAIL'>
};

type ListElementType = {
    name: string; 
    value?: string;
}

export default ({route, navigation}: Props) => {
    const {colors} = useTheme();
    const {itemId} = route.params;
    const [isLoading, setIsLoading] = useState(true);
    const [isError, setIsError] = useState(false);
    const [requestLoading, setRequestLoading] = useState(true);
    const [taskElement, setTaskElement] = useState<SectionListData<ListElementType>[]>([]);
    const fetchWithToken = useFetchWithToken();

    useEffect(() => {
        if(requestLoading){

            setIsError(false)
            setIsLoading(true);

            fetchWithToken(`/task/${itemId}`)
                .then((res): Promise<TaskType> => {
                    return res.json()
                })
                .then((taskData):SectionListData<ListElementType>[]  => {
    
                    const createdAt =  new Date(taskData.createdAt);
                    const dateArray = [];
    
                    return [
                        {
                            data: [
                                {name: "URL", value: `${taskData.mainArgument}`},
                                {name: "TYPE", value: i18n.t(`${taskData.type}`, {defaultValue: `${taskData.type}`})}
                            ]
                        },
                        {
                            title: "STATE",
                            data: [
                                {name: "STATUS", value: i18n.t(`taskStatus.${TaskStatus[taskData.status]}`, {defaultValue: TaskStatus[taskData.status]})}
                            ]
                        },
                        {
                            title: "Date",
                            data: [{name: "CREATEDAT", value: `${createdAt.toLocaleDateString()} ${createdAt.toLocaleTimeString()}`}]
                        }
                    ]
                })
                .then(sectionListData => setTaskElement(sectionListData))
                .catch(err => {
                    console.log(err.message);
                    setIsError(true)
                })
                .finally(() => setIsLoading(false));

            setRequestLoading(false);
        }
    }, [requestLoading]);

    return (
        <>
            {isError && <ErrorLoading onReload={() => setRequestLoading(true)} />}
            {!isError && isLoading && (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator color={colors.secondary}/>
                    <Text style={{color: colors.secondary, marginTop: 8}}>{i18n.t("LOADING")}</Text>
                </View>
            )}
            {!isError && !isLoading && (
                <SectionList
                style={styles.mainContainer}
                keyExtractor={item => item.name}
                sections={taskElement}
                renderSectionFooter={() => (
                    <View style={{height: 20}}/>
                )}
                renderSectionHeader={({ section: { title } }) => (
                    <Text style={{...styles.headerSection, color: colors.secondary}}>{title && i18n.t(title, {defaultValue: title})}</Text>
                )}
                renderItem={({item, section: { title }}) => (
                    <View style={{...styles.touchableItem, backgroundColor: colors.card}}>
                        <Text 
                        style={{...styles.leftElement, color: colors.text }}
                        >
                            {i18n.t(item.name, {defaultValue: item.name})}
                        </Text>
                        <Text style={{...styles.leftElement, color: colors.secondary}}>{item.value}</Text>
                        
                    </View>
                )}
                ItemSeparatorComponent={() => (
                    <View style={{backgroundColor: colors.card}}>
                        <View style={{borderBottomWidth: 0.5, borderBottomColor: colors.border, marginLeft: 20}}/>
                    </View>
                )}/>
            )}
        </>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
    },
    headerSection: { 
      marginVertical: 11, 
      paddingHorizontal: 20 
    },
    touchableItem: {
        paddingVertical: 11, 
        paddingHorizontal: 20,    
        flex: 1,
        flexDirection: "row", 
        justifyContent: "space-between"
    },
    leftElement: {
      fontSize: 17,
      fontWeight: '400'
    },
    logoutButton: {
  
    }
  })