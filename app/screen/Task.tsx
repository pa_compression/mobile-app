import React, { useEffect, useState } from 'react';
import { useTheme, useFocusEffect } from '@react-navigation/native';
import { View, Text, SectionList, TouchableHighlight, StyleSheet, Button, SectionListData } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import i18n from '../utils/i18n';
import { useFetchWithToken } from '../utils/query';
import { TaskType, TaskStatus } from '../constants/service';
import ErrorLoading from '../components/ErrorLoading';

export default ({navigation}) => {
    const { colors } = useTheme();
    const [taskElement, setTaskElement] = useState<SectionListData<TaskType>[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [requestLoading, setRequestLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const fetchWithToken = useFetchWithToken();

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if(!requestLoading){
                setRequestLoading(true);
                setIsLoading(true);
            }
        });

        return unsubscribe;
      }, [navigation]);
      
    useEffect(() => {

        if(requestLoading){

            setIsError(false)
            setIsLoading(true);
            fetchWithToken('/task')
                .then((response): Promise<TaskType[]> => {
                    // console.log(response.status);
                    if(!response.ok)

                        throw new Error("Cannot get your content")

                    return response.json()
                })
                .then(data => {
                    setTaskElement([
                        {
                            title: "RUNNING",
                            data: data.filter(taskElement => taskElement.status === TaskStatus.RUNNING)
                        },
                        {
                            title: "WAITING",
                            data: data.filter(taskElement => taskElement.status === TaskStatus.WAITING)
                        },
                        {
                            title: "END",
                            data: data.filter(taskElement => taskElement.status === TaskStatus.END)
                        },
                        {
                            title: "ERROR",
                            data: data.filter(taskElement => taskElement.status === TaskStatus.ERROR)
                        }
                    ].filter(element => element.data.length > 0));
    
                    setIsLoading(false)
                })
                .catch(err => {
                    // console.log(err.message);
                    setIsError(true)
                    setIsLoading(false)
                });
    
                setRequestLoading(false);
        }


    }, [requestLoading]);

    return (
        <>
            {isError && <ErrorLoading onReload={() => setRequestLoading(true)} />}
            {!isError && (
                <SectionList
                style={styles.mainContainer}
                keyExtractor={item => item.id}
                sections={taskElement}
                renderSectionFooter={() => (
                    <View style={{height: 20}}/>
                )}
                onRefresh={() => setRequestLoading(true)}
                refreshing={isLoading || requestLoading}
                renderSectionHeader={({ section: { title } }) => (
                    <Text style={{...styles.headerSection, color: colors.secondary}}>{title && i18n.t(`taskStatus.${title}`, {defaultValue: title})}</Text>
                )}
                renderItem={({item}) => (
                    <TouchableHighlight 
                        style={{ ...styles.touchable, backgroundColor: colors.card}}
                        underlayColor="rgba(199, 199, 204, 0.2)"
                        onPress={event => navigation.navigate("TASK_DETAIL", {itemId: item.id})}
                    >
                        <View style={{flex: 1, flexDirection: "row", alignItems: "center"}}>
                            <View style={styles.touchableItem}>
                                <Text 
                                style={{...styles.leftElement, color: colors.text }}
                                >
                                    {item.name}
                                </Text>
                                {/* {item.status === TaskStatus.RUNNING && (
                                    <Text style={{...styles.leftElement, color: colors.secondary}}>{`${item.progression || 0} %`}</Text>
                                )} */}
                                
                            </View>
                            <Ionicons name="ios-arrow-forward" size={17} color={colors.secondary}/>
                        </View>
                    </TouchableHighlight>
                )}
                ListEmptyComponent={() => (
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <Text  style={{color: colors.secondary}}>{i18n.t("NO_TASK")}</Text>
                    </View>
                )}
                ItemSeparatorComponent={() => (
                    <View style={{backgroundColor: colors.card}}>
                    <View style={{borderBottomWidth: 0.5, borderBottomColor: colors.border, marginLeft: 20}}/>
                    </View>
                )}
                />
            )}
        </>
    );
}

const styles = StyleSheet.create({
    mainContainer: {
    },
    headerSection: { 
      marginVertical: 11, 
      paddingHorizontal: 20 
    },
    touchable: {
      paddingVertical: 11, 
      paddingHorizontal: 20 
    },
    touchableItem: {
      flex: 1, 
      marginRight: 10,
      flexDirection: "row", 
      justifyContent: "space-between"
    },
    leftElement: {
      fontSize: 17,
      fontWeight: '400'
    },
    logoutButton: {
  
    }
  })