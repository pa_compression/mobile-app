import React, {useState, useEffect } from 'react';
import { Text, View, FlatList, StyleSheet, TouchableHighlight } from 'react-native';
import { BottomTabBarProps } from '@react-navigation/bottom-tabs';
import { useTheme } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import i18n from '../utils/i18n';
import { FileType } from '../constants/service';
import ErrorLoading from '../components/ErrorLoading';
import { useFetchWithToken } from '../utils/query';

type Props = {
    navigation: BottomTabBarProps;
}

export default function Home({navigation}: Props){
    const { colors } = useTheme();
    const [fileElement, setFileElement] = useState<FileType[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [requestLoading, setRequestLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const fetchWithToken = useFetchWithToken();

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if(!requestLoading){
                setRequestLoading(true);
                setIsLoading(true);
            }
        });

        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        if(requestLoading === true){
            
            setIsError(false)
            setIsLoading(true);
            fetchWithToken(`/file`)
                .then((response): Promise<FileType[]> => {
                    return response.json()
                })
                .then(data => {
                    setFileElement(data);
    
                    setIsLoading(false)
                })
                .catch(err => {
                    console.log(err.message);
                    setIsError(true)
                    setIsLoading(false)
                });
    
                setRequestLoading(false);
        }

    }, [requestLoading]);

    return (
        <>
            {isError && <ErrorLoading onReload={() => setRequestLoading(true)} />}
            {!isError && ( 
                <FlatList 
                    style={{marginVertical: 40}}
                    data={fileElement}
                    refreshing={isLoading || requestLoading}
                    keyExtractor={item => item.id}
                    onRefresh={() => setRequestLoading(true)}
                    ListEmptyComponent={() => (
                        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                            <Text  style={{color: colors.secondary}}>{i18n.t("NO_FILE")}</Text>
                        </View>
                    )}
                    ItemSeparatorComponent={() => (
                        <View style={{backgroundColor: colors.card}}>
                            <View style={{borderBottomWidth: 0.5, borderBottomColor: colors.border, marginLeft: 20}}/>
                        </View>
                    )}
                    renderItem={({item}) => (
                        <TouchableHighlight 
                            style={{ ...styles.touchable, backgroundColor: colors.card}}
                            underlayColor="rgba(199, 199, 204, 0.2)"
                            onPress={_event => navigation.navigate("FILE_DETAIL", {itemId: item.id})}
                        >
                            <View style={styles.touchableItem}>
                                <Text 
                                style={{...styles.leftElement, color: colors.text }}
                                >
                                    {item.name}
                                </Text>
                                <Ionicons name="ios-arrow-forward" size={17} color={colors.secondary}/>
                            </View>
                        </TouchableHighlight>
                    )}
            />)}
        </>
    );
}

const styles = StyleSheet.create({
    touchable: {
        paddingVertical: 11, 
        paddingHorizontal: 20 
    },
    touchableItem: {
        flex: 1, 
        marginRight: 10,
        flexDirection: "row", 
        justifyContent: "space-between"
    },
    leftElement: {
        fontSize: 17,
        fontWeight: '400'
    },
});