import React, { useState, useEffect } from 'react';
import { useTheme, RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import * as Yup from 'yup';
import { View, Text, StyleSheet, TextInput, ScrollView, Alert, ActivityIndicator, TouchableHighlight } from 'react-native';
import Border from '../components/Border';
import { RootStackParamList } from '../constants/navigation';
import i18n from '../utils/i18n';
import { useFetchWithToken } from '../utils/query';

type Props = {
    navigation: StackNavigationProp<RootStackParamList, "TASK_CREATE">;
    route: RouteProp<RootStackParamList, "TASK_CREATE">;
}

type FormElement = {
    name?: string;
    mainArgument?:string;
}

Yup.setLocale({
    string: {
        url: 'field.string.url'
    },
    mixed: {
        required: 'field.required'
    }
})

const toValidate = Yup.object({
    name: Yup.string()
        .trim()
        .required(),
    mainArgument: Yup.string()
        .trim()
        .required()
})

const formValidate = toValidate.shape({
    mainArgument: Yup.string()
        .trim()
        .url()
        .required()
})

export default function TaskCreate({navigation}: Props) {
    const { colors } = useTheme();
    const [isField, setIsField] = useState(false);
    const [formElement, setFormElement] = useState<FormElement>({
        mainArgument: "https://"
    });
    const fetchWithToken = useFetchWithToken();

    function onValueChange(input: string, text: string){
        setFormElement({
            ...formElement,
            [input]: text
        });
    }

    function onSubmit(){

        if(isField) {

            formValidate.validate(formElement)
                .then((formValue) => {
                    navigation.setOptions({headerRight: () => <ActivityIndicator/>})

                    fetchWithToken(`/task`, {
                        method: "POST",
                        body: JSON.stringify(formValue),
                        headers: {
                            'Content-Type': "application/json"
                        }
                    })
                        .then(response => {
                            if(response.ok) navigation.goBack();
                            else Alert.alert(i18n.t("ERROR"), i18n.t("ERROR_SAVE_DATA"))
                        })
                        .catch(error => Alert.alert(i18n.t("ERROR"), i18n.t("ERROR_SAVE_DATA")))
                        .finally(() => navigation.setOptions({headerRight: () => getHeaderRight()}))

                    console.log(formValue);
                })
                .catch((err: Yup.ValidationError) => {

                    const firstError = err.errors.pop();

                    if(firstError) Alert.alert(
                        i18n.t("INVALID_FORM"),
                        i18n.t(firstError, {defaultValue: "We are not able to validate your form"})
                    )
                })

            
        }
    }

    function getHeaderRight(){

        return <Text 
            style={{color: isField ? colors.primary : colors.border, fontSize: 17, paddingVertical: 10, paddingLeft: 10}}
            onPress={onSubmit}
        >
            {i18n.t("DONE", {defaultValue: "DONE"})}
        </Text>
    }

    useEffect(() => {

        toValidate.isValid(formElement)
            .then(isValid => {
                setIsField(isValid)
            })
            .catch(() => setIsField(false))

    }, [formElement])

    navigation.setOptions({headerRight: () => getHeaderRight()})

    return <ScrollView style={styles.container}>
        <View style={{...styles.element, backgroundColor: colors.card}}>
            <TextInput style={styles.input} 
                placeholder={i18n.t('NAME', {defaultValue: "NAME"})} 
                onChangeText={value => onValueChange('name', value)}
                value={formElement.name || ''}
            />
        </View>
        <Border />
        <View style={{...styles.element, backgroundColor: colors.card}}>
            <TextInput 
                style={styles.input} 
                placeholder="Url" 
                onChangeText={value => onValueChange('mainArgument', value)}
                value={formElement.mainArgument || ''}
            />
        </View>
    </ScrollView>
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 40
    },
    element: {
        paddingHorizontal: 20 
    },
    input: {
        height: 40, 
        fontSize: 17
    }
})