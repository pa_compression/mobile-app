import React, { useEffect, useState } from 'react';
import { View, Text, ActivityIndicator, StyleSheet, SectionList, SectionListData, TouchableHighlight, Alert, Share } from 'react-native';
import * as Linking from 'expo-linking';
import { useTheme } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { FileType, TaskType } from '../constants/service';
import { Ionicons } from '@expo/vector-icons';
import { useFetchWithToken } from '../utils/query';
import { RootStackParamList } from '../constants/navigation';
import ErrorLoading from '../components/ErrorLoading';
import { apiHost } from '../constants/service';
import i18n from '../utils/i18n';


type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'FILE_DETAIL'
>;

type Props = {
  navigation: ProfileScreenNavigationProp;
  route: RouteProp<RootStackParamList, 'FILE_DETAIL'>
};

type ListElementType = {
    name: string; 
    value?: string;
    link?: (value?: string) => void;
}

export default ({route, navigation}: Props) => {
    const {colors} = useTheme();
    const {itemId} = route.params;
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [requestLoading, setRequestLoading] = useState(false);
    const [taskElement, setTaskElement] = useState<SectionListData<ListElementType>[]>([]);
    const fetchWithToken = useFetchWithToken();

    function getDownloadLink(id: string, type: string){

        return `${apiHost}/file/download/${id}/${type == "COMPRESS" ? "compress": "uncompress"}`
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if(!requestLoading){
                setRequestLoading(true);
                setIsLoading(true);
            }
        });

        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        if(requestLoading){
            setIsLoading(true);
            fetchWithToken(`/file/${itemId}`)
                .then((res): Promise<FileType> => res.json())
                .then((fileData):SectionListData<ListElementType>[]  => {
    
                    const createdAt =  new Date(fileData.createdAt);
                    const taskArray = [];

                    if(fileData.uncompressTaskId !== null)

                        taskArray.push({
                            name: "TASK_UNCOMPRESS", 
                            value: fileData.uncompressTaskId, 
                            link: (taskId) => navigation.navigate("TASK_DETAIL", {itemId: taskId})
                        })

                    else

                        taskArray.push({name: "UNCOMPRESS", value: fileData.taskId, link: () => createUnCompressTask()})

                    if(fileData.uncompressTaskAvailable)

                        taskArray.push({
                            name: "DOWNLOAD_UNCOMPRESS", 
                            value: fileData.id, 
                            link: (id) => Linking.openURL(getDownloadLink(id, "UNCOMPRESS"))
                        })
    
                    return [
                        {
                            data: [
                                {name: "NAME", value: `${fileData.name}`}
                            ]
                        },
                        {
                            data: [
                                {name: "CREATEDAT", value: `${createdAt.toLocaleDateString()} ${createdAt.toLocaleTimeString()}`}
                            ]
                        },
                        {
                            data: [
                                {
                                    name: "TASK_COMPRESS", 
                                    value: fileData.taskId, 
                                    link: (taskId) => navigation.navigate("TASK_DETAIL", {itemId: taskId})
                                },
                                {
                                    name: "DOWNLOAD_COMPRESS", 
                                    value: fileData.id, 
                                    link: (id) => Linking.openURL(getDownloadLink(id, "COMPRESS"))
                                },
                                {
                                    name: "SHARE_COMPRESS", 
                                    value: fileData.id, 
                                    link: (id) => Share.share({message: `${i18n.t("DOWNLOAD_MY_FILE")} ${apiHost}/file/download/${id}/compress`})
                                },
                                
                            ].concat(taskArray)
                        },
                        {
                            data: [
                                {name: "DELETE", value: fileData.id, link: deleteRequest},                                
                            ]
                        }
                    ]
                })
                .then(sectionListData => setTaskElement(sectionListData))
                .catch(err => {
                    console.log(err.message);
                    setIsError(true)
                })
                .finally(() => setIsLoading(false));

            setRequestLoading(false);
        }
    }, [requestLoading]);

    const deleteRequest = (fileId?: string) => {

        Alert.alert(
            i18n.t("WARNING"),
            i18n.t("DELETE_FILE_WARNING"),
            [
                {
                    text: "Ok",
                    onPress: () => {

                        fetchWithToken(`/file/${fileId}`, {method: 'delete'})
                            .then(response => {
                                if(response.ok)

                                    navigation.goBack();
                                else 

                                    Alert.alert(i18n.t("ERROR_TITLE"), i18n.t("DELETE_ERROR_TEXT"), [{text: "Ok"}])

                            }).catch(() => 
                                Alert.alert(i18n.t("ERROR_TITLE"), i18n.t("DELETE_ERROR_TEXT"), [{text: "Ok"}])
                            )
                        
                    }
                },
                {
                    text: "Cancel"
                }
            ]
        )
    }

    function createUnCompressTask(){

        setIsLoading(true);

        fetchWithToken(`/file/${itemId}/uncompress`)
            .then((res): Promise<TaskType> => res.json())
            .then(task => {

                setRequestLoading(true);

                Alert.alert(
                    i18n.t("UNCOMPRESS_TASK_CREATE_TITLE"), 
                    i18n.t("UNCOMPRESS_TASK_CREATE_TEXT"),
                    [
                        {
                            text: i18n.t("TASK_DETAIL"),
                            onPress: () => navigation.navigate("TASK_DETAIL", {itemId: task.id})
                        },
                        {
                            text: "Ok"
                        }   
                    ]
                )
            })
            .catch(error => {

                setIsLoading(false);
                Alert.alert(
                    i18n.t("UNCOMPRESS_TASK_CREATE_ERROR_TITLE"),
                    i18n.t("UNCOMPRESS_TASK_CREATE_ERROR_TEXT")
                )
            });
    }

    // console.log(itemId);

    return (
        <>
            {isError && <ErrorLoading onReload={() => setRequestLoading(true)} />}
            {!isError && isLoading && (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator color={colors.secondary}/>
                    <Text style={{color: colors.secondary, marginTop: 8}}>{i18n.t("LOADING")}</Text>
                </View>
            )}
            {!isError && !isLoading && (
                <SectionList
                style={styles.mainContainer}
                keyExtractor={item => item.name}
                sections={taskElement}
                renderSectionFooter={() => (
                    <View style={{height: 20}}/>
                )}
                renderSectionHeader={({ section: { title } }) => (
                    <Text style={{...styles.headerSection, color: colors.secondary}}>{title && i18n.t(title, {defaultValue: title})}</Text>
                )}
                renderItem={({item, section: { title }}) => (
                    <>
                        {!item.link && (
                            <View style={{...styles.touchableItem, backgroundColor: colors.card}}>
                                <Text 
                                style={{...styles.leftElement, color: colors.text }}
                                >
                                    {i18n.t(item.name, {defaultValue: item.name})}
                                </Text>
                                <Text style={{...styles.leftElement, color: colors.secondary}}>{item.value}</Text>
                        
                            </View>
                        )}

                        {item.link && (
                            <TouchableHighlight 
                                style={{backgroundColor: colors.card}}
                                underlayColor="rgba(199, 199, 204, 0.2)"
                                onPress={_event => item.link(item.value)}
                            >
                                <View style={styles.touchableItem}>
                                    <Text 
                                    style={{...styles.leftElement, color: colors.text }}
                                    >
                                        {i18n.t(item.name, {defaultValue: item.name})}
                                    </Text>
                                    <Ionicons name="ios-arrow-forward" size={17} color={colors.secondary}/>
                                </View>
                            </TouchableHighlight>
                        )}
                    </>
                )}
                ItemSeparatorComponent={() => (
                    <View style={{backgroundColor: colors.card}}>
                        <View style={{borderBottomWidth: 0.5, borderBottomColor: colors.border, marginLeft: 20}}/>
                    </View>
                )}/>
            )}
        </>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
    },
    headerSection: { 
      marginVertical: 11, 
      paddingHorizontal: 20 
    },
    touchableItem: {
        paddingVertical: 11, 
        paddingHorizontal: 20,    
        flex: 1,
        flexDirection: "row", 
        justifyContent: "space-between"
    },
    leftElement: {
      fontSize: 17,
      fontWeight: '400'
    },
    logoutButton: {
  
    }
  })