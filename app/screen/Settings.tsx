import React from 'react';
import { Text, View, Button, StyleSheet, SectionList, TouchableHighlight, SectionListData } from 'react-native';
import { connect } from 'react-redux';
import { useTheme } from '@react-navigation/native';
import i18n from '../utils/i18n';
import { setToken } from '../store/auth/action';
import {ApplicationState} from '../store/index';
import RevokeAccessToken from '../utils/logout';
import {baseConfig as BaseAuthConfig} from '../constants/authProvider';

const mapState = (state: ApplicationState) => ({
  token: state.auth.token
});

type Props = {
  setToken: typeof setToken;
} & ReturnType<typeof mapState>

type SettingsArrayObject = {name: string; value: string};

const Settings = function({setToken, token}: Props) {
    const { colors } = useTheme();
    const dateExpired = token.expires ? new Date(token.expires) : new Date();
    const settingsParam = [
      {
        title: "ACCOUNT",
        data: [
          {"name": "EMAIL", value: token.tokenDecode?.email},
          {"name": "NAME", value: token.tokenDecode?.name},
        ]
      },
      {
        title: "AUTHENTICATION",
        data: [
          {"name": "EXPIRES", value: `${dateExpired.toLocaleDateString()} ${dateExpired.toLocaleTimeString()}`}
        ]
      },
      {
        title: null,
        data: [
          {"name": "LOGOUT", "color": "red", "onClick": logout}
        ]
      },
    ] as SectionListData<{
        name: string; 
        value?: string, 
        color?: string; 
        onClick?: () => void;
    }>[];


    async function logout(){

      if(token.refresh_token)
        try {
          await RevokeAccessToken(BaseAuthConfig, token.refresh_token);
          setToken({isAuth: false});
        }catch(e){

          console.log(e);
        }
    }

    return (
        <SectionList
          style={styles.mainContainer}
          keyExtractor={item => item.name}
          sections={settingsParam}
          renderSectionFooter={() => (
            <View style={{height: 20}}/>
          )}
          renderSectionHeader={({ section: { title } }) => (
            <Text style={{...styles.headerSection, color: colors.secondary}}>{title && i18n.t(title, {defaultValue: title})}</Text>
          )}
          renderItem={({item}) => (
            <TouchableHighlight 
              style={{ ...styles.touchable, backgroundColor: colors.card}}
              underlayColor="rgba(199, 199, 204, 0.2)"
              onPress={() => item.onClick ? item.onClick() : null}
            >
              <View style={styles.touchableItem}>
                <Text 
                  style={{...styles.leftElement, color: item.color ? item.color : colors.text }}
                >
                  {i18n.t(item.name, {defaultValue: item.name})}
                </Text>
                
                {item.value && (
                  <Text style={{...styles.leftElement, color: colors.secondary}}>{item.value}</Text>
                )
                }
                
              </View>
            </TouchableHighlight>
          )}
          
          ItemSeparatorComponent={() => (
            <View style={{backgroundColor: colors.card}}>
              <View style={{borderBottomWidth: 0.5, borderBottomColor: colors.border, marginLeft: 20}}/>
            </View>
          )}
        />
    );
}

const styles = StyleSheet.create({
  mainContainer: {
  },
  headerSection: { 
    marginVertical: 11, 
    paddingHorizontal: 20 
  },
  touchable: {
    paddingVertical: 11, 
    paddingHorizontal: 20 
  },
  touchableItem: {
    flex: 1, 
    flexDirection: "row", 
    justifyContent: "space-between"
  },
  leftElement: {
    fontSize: 17,
    fontWeight: '400'
  },
  logoutButton: {

  }
})

export default connect(mapState, {setToken})(Settings);