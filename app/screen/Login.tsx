import React, {useEffect} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';
import { useTheme } from '@react-navigation/native';
import { StackNavigationProp   } from '@react-navigation/stack';
import * as AppAuth from 'expo-app-auth';
import { connect } from 'react-redux';
import i18n from '../utils/i18n';
import { Ionicons } from '@expo/vector-icons';
import { setToken } from '../store/auth/action';
import jwtDecode  from 'jwt-decode';
import {config as AuthConfig} from '../constants/authProvider';

type LoginPropsType = {
    navigation: StackNavigationProp<{}>;
    setToken: typeof setToken;
}

const Login =  function({setToken}: LoginPropsType) {
    const { colors } = useTheme();

    async function InitKeycloak(props: LoginPropsType) {

        try{
            const tokenResponse = await AppAuth.authAsync(AuthConfig);

            if(tokenResponse.accessToken && tokenResponse.refreshToken)
                setToken({
                    isAuth: true,
                    expires: tokenResponse.accessTokenExpirationDate ? tokenResponse.accessTokenExpirationDate : new Date().toString(),
                    refresh_token: tokenResponse.refreshToken,
                    token: tokenResponse.accessToken,
                    tokenDecode: jwtDecode(tokenResponse.accessToken)
                });

        }catch(e){
            console.log(typeof e);
            console.log(e.code);
            console.log(e.name);
        }
    }

    return <View style={styles.baseContainer}>
        <View style={styles.titleContainer}>
            <Ionicons name="ios-folder" size={120} color="tomato" />
            <Text style={{...styles.title, color: colors.text}}>TCD</Text>
            <Text style={{color: colors.text, textAlign: "center"}}>{ i18n.t("LOGIN_MESSAGE") }</Text>
        </View>
        <View style={styles.buttonContainer}>
            <Button
                onPress={InitKeycloak}
                title={ i18n.t("LOGIN") }
                color={colors.primary}
                accessibilityLabel="Login in the app"
            />
        </View>
    </View>
}

const styles = StyleSheet.create({
    baseContainer: {
        flex: 1,
        padding: 40
    },
    titleContainer: {
        flex: 3,
        alignItems: "center",
        justifyContent: "center"
    },
    title: {
        fontSize: 40,
        margin: 30
    },
    buttonContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end"
    }
})

export default connect(null, {setToken})(Login);