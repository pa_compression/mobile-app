import { AuthState, SET_TOKEN, AuthActionTypes} from './type';

const initialState: AuthState = {
    token: {
        isAuth: false
    },
}

export default (state = initialState, action: AuthActionTypes): AuthState => {

    switch(action.type) {
        
        case SET_TOKEN:
            return {
                token: action.token
            }
        default:
            return state;
    }
}