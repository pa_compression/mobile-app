import { Token, SET_TOKEN, AuthActionTypes} from './type';

export const setToken = (token: Token): AuthActionTypes => ({
    type: SET_TOKEN,
    token
});