export const SET_TOKEN = 'SET_TOKEN';

export type Token = {
    token?: string;
    refresh_token?: string;
    expires?: string;
    isAuth: boolean;
    tokenDecode?: {
        email: string;
        name: string;
    };
}

export type AuthState = {
    token: Token;
}

type SetTokenAction = {
    type: typeof SET_TOKEN;
    token: Token;
}

export type AuthActionTypes = SetTokenAction;
