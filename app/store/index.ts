import { combineReducers } from 'redux';
import authReducer from './auth/reducer'
import {AuthState} from './auth/type'

export type ApplicationState = {
    auth: AuthState
};

export const rootReducer = combineReducers<ApplicationState>({
    auth: authReducer
});

