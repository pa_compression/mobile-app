import React from 'react';
import { useTheme } from '@react-navigation/native';
import { View, Text, TouchableHighlight} from 'react-native';

type Props = {
    onReload: () => void;
}

export default function ErrorLoading(props: Props) {
    const { onReload } = props;
    const { colors } = useTheme();

    return (
        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
            <Text  style={{color: colors.secondary}}>Cannot get your content</Text>
            <TouchableHighlight onPress={onReload}>
                <Text style={{
                    borderColor: colors.secondary, 
                    borderWidth: 0.5, 
                    borderRadius: 4,
                    color: colors.secondary, 
                    paddingHorizontal: 30, 
                    paddingVertical: 5,
                    marginTop: 10
                }}>Retry</Text>
            </TouchableHighlight>
        </View>
    )
}