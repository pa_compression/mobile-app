import React from 'react';
import { View } from 'react-native';
import { useTheme } from '@react-navigation/native';

export default function Border() {
    const { colors } = useTheme();

    return <View style={{backgroundColor: colors.card}}>
        <View style={{borderBottomWidth: 0.5, borderBottomColor: colors.border, marginLeft: 20}}/>
    </View>
}