import React, {useState} from 'react';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets  } from '@react-navigation/stack';
import { AppearanceProvider, useColorScheme } from 'react-native-appearance';
import { connect } from 'react-redux';
import { StatusBar, View, StyleSheet, Platform, Button } from 'react-native';
import BottomTabNavigator from './BottomTabNavigator';
import TaskDetail from '../screen/TaskDetail';
import FileDetail from '../screen/FileDetail';
import TaskCreate from '../screen/TaskCreate';
import Login from '../screen/Login';
import i18n from '../utils/i18n';
import {ApplicationState} from '../store/index'
import Task from '../screen/Task';

const OwnDefaultTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        secondary: 'rgb(142, 142, 147)'
    }
}

const OwnDarkTheme = {
    ...DarkTheme,
    colors: {
        ...DarkTheme.colors,
        secondary: 'rgb(142, 142, 147)'
    }
}

const Stack = createStackNavigator();

const mapState = (state: ApplicationState) => ({
    token: state.auth.token
})

type RootPropsType = ReturnType<typeof mapState>

const Root = function(props: RootPropsType) {
    const scheme = useColorScheme();
    const {token: {
        isAuth
    }} = props;

    return (
        <AppearanceProvider>
            <View style={styles.container}>
                {Platform.OS === 'ios' && <StatusBar barStyle={scheme === 'dark' ? "light-content" : "dark-content"}/>}
                <NavigationContainer theme={scheme === 'dark' ? OwnDarkTheme : OwnDefaultTheme}>
                    <Stack.Navigator mode="modal" screenOptions={{gestureEnabled: true, headerRightContainerStyle: {marginRight: 15}}}>
                    {isAuth ? (
                        <>
                            <Stack.Screen name="HOME" component={BottomTabNavigator} />
                            <Stack.Screen name="FILE_DETAIL" 
                                component={FileDetail}
                                options={{title: i18n.t("route.FILE_DETAIL")}}
                            />
                            <Stack.Screen name="TASK_DETAIL" 
                                component={TaskDetail}
                                options={{title: i18n.t("route.TASK_DETAIL")}}
                            />
                            <Stack.Screen name="TASK_CREATE" 
                                component={TaskCreate}
                                options={{title: i18n.t("route.TASK_CREATE")}}
                            />
                        </>
                    ) : (
                        <Stack.Screen name="TITLE" component={Login} options={{headerShown: false, title: i18n.t("LOGIN_TITLE")}}/>
                    )
                    }
                    
                    </Stack.Navigator>
                </NavigationContainer>
            </View>
        </AppearanceProvider>)
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

export default connect<ReturnType<typeof mapState>, {}, {}, ApplicationState>(
    mapState
)(Root);