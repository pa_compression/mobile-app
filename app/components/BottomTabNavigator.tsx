import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { StackNavigationProp, StackNavigationOptions } from '@react-navigation/stack';
import { RouteProp, useTheme } from '@react-navigation/native';
import React from 'react';
import {Text} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import routes from '../constants/routes';
import i18n from '../utils/i18n';
import { RootStackParamList } from '../constants/navigation';


const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'FOLDER';

type Props = {
  navigation: StackNavigationProp<RootStackParamList, "HOME">;
  route: RouteProp<RootStackParamList, "HOME">;
}

export default function BottomTabNavigator({ navigation, route }: Props) {

  navigation.setOptions(getHeaderOption(route, navigation, useTheme()));

  return (
    <BottomTab.Navigator 
        initialRouteName={INITIAL_ROUTE_NAME}
        tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
          }}
    >
        {routes.bottomBar.map(element => (
            <BottomTab.Screen
            name={element.title}
            key={element.title}
            component={element.component}
            options={{
              title: i18n.t(`route.bottomBar.${element.title}`, {defaultValue: element.title}),
              tabBarIcon: ({ focused, color, size }) => <Ionicons name={element.icon} size={size} color={color} />,
            }}
          />
        ))}
    </BottomTab.Navigator>
  );
}

function getHeaderOption(
  route: RouteProp<RootStackParamList, "HOME"> & {state?: { index: number; routes: {name: string}[]};},
  navigation: StackNavigationProp<RootStackParamList, "HOME">,
  theme: ReturnType<typeof useTheme>
): Partial<StackNavigationOptions> {
  const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;
  const { colors } = theme;
  const find = routes.bottomBar.find(element => routeName === element.title);

  return { 
    headerTitle: i18n.t(find ? `route.bottomBarHeader.${find.headerTitle}`: 'route.DEFAULT', {defaultValue: find?.headerTitle}), 
    headerRight: () => {

      if(find?.title !== "SETTINGS")
        return <Text style={{color: colors.primary, fontSize: 17}} onPress={() => navigation.navigate("TASK_CREATE")}>
                {i18n.t("CREATE", {defaultValue: "CREATE"})}
              </Text>

      return <></>
    }
  }
}
