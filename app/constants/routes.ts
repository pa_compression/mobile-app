import React from 'react';
import HomeScreen from '../screen/Home';
import SettingsScren from '../screen/Settings';
import TaskScreen from '../screen/Task';

export default {
    bottomBar: [
        {headerTitle: 'FOLDER', component: HomeScreen, icon: "ios-folder-open", title: "FOLDER"},
        {headerTitle: 'TASK', component: TaskScreen, icon: "ios-hammer", title: "TASK"},
        {headerTitle: 'SETTINGS', component: SettingsScren, icon: "ios-settings", title: "SETTINGS"}
    ]
} as {
    bottomBar: Array<{
        headerTitle: string;
        icon: string;
        title: string;
        component: React.ComponentType<any>;
    }>
}