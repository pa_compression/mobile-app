import {OAuthBaseProps, OAuthProps} from 'expo-app-auth';

const BASE_URL = "https://esgi-jee-keycloak.herokuapp.com/auth/realms/projet-annuel";

export const  baseConfig = {
    issuer: BASE_URL,
    clientId: "mobile-app",
    serviceConfiguration: {
        tokenEndpoint: `${BASE_URL}/protocol/openid-connect/token`,
        revocationEndpoint: `${BASE_URL}/protocol/openid-connect/logout`,
        registrationEndpoint: `${BASE_URL}/clients-registrations/openid-connect`,
        authorizationEndpoint: `${BASE_URL}/protocol/openid-connect/auth`
    }
} as OAuthBaseProps

export const config = {
    ...baseConfig,
    scopes: ["openid", "offline_access", "roles", "email", "profile"]
} as OAuthProps