export type RootStackParamList = {
    HOME: undefined;
    TASK_DETAIL: { itemId: string | undefined };
    LOGIN: undefined;
    TASK_CREATE: undefined;
    FILE_DETAIL: { itemId: string | undefined };
};