export const apiHost = "http://esgi-pa-api-261421812.eu-west-3.elb.amazonaws.com"

type ServiceObjectType = {
    _id: string;
    id: string;
    createdAt: string;
    updatedAt: string;
}

export enum TaskStatus {
    WAITING,
    RUNNING,
    END,
    ERROR
}

export type TaskType = ServiceObjectType & {
    name: string;
    mainArgument: number;
    status: number;
    type: string;
}

export type FileType = ServiceObjectType & {
    name: string;
    taskId: string;
    uncompressTaskId: string;
    createdAt: string;
    folderUuid: string;
    uncompressTaskAvailable: boolean;
}